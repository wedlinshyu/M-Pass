### 博学，审问，慎思，明辨，笃行
> 唯独坚持，方能成就

## 更新记录链接
- [x] [二零一九年十月更新记录](https://gitee.com/ibyte/M-Pass/blob/master/update-record/UPDATE-RECORD-2019-10.md)
- [x] [二零一九年十一月更新记录](https://gitee.com/ibyte/M-Pass/blob/master/update-record/UPDATE-RECORD-2019-11.md)
- [x] [二零一九年十二更新记录](https://gitee.com/ibyte/M-Pass/blob/master/update-record/UPDATE-RECORD-2019-12.md)
- [x] [二零二零年一月更新记录](https://gitee.com/ibyte/M-Pass/blob/master/update-record/UPDATE-RECORD-2020-01.md)
- [x] [二零二零年二月更新记录](https://gitee.com/ibyte/M-Pass/blob/master/update-record/UPDATE-RECORD-2020-02.md)
- [x] [二零二零年三月更新记录](https://gitee.com/ibyte/M-Pass/blob/master/update-record/UPDATE-RECORD-2020-03.md)
- [ ] [二零二零年四月更新记录](https://gitee.com/ibyte/M-Pass/blob/master/update-record/UPDATE-RECORD-2020-04.md)